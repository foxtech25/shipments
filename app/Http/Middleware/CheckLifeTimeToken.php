<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redis;

/**
 * Class CheckLifeTimeToken
 * @package App\Http\Middleware
 */
class CheckLifeTimeToken
{
    /**
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Redis::get('token')) {
            return redirect('/login');
        }

        return $next($request);
    }
}
