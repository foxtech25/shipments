<?php
/**
 * Created by PhpStorm.
 * User: foxtech
 * Date: 17.06.18
 * Time: 16:05
 */

namespace App\Http\Controllers;

use App\Adapters\UserAdapter;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @param Request $request
     * @param UserAdapter $adapter
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login(Request $request, UserAdapter $adapter): RedirectResponse
    {
        $adapter->login(
            $request->email,
            $request->password
        );

        return redirect('/shipment');
    }
}