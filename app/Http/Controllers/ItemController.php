<?php
/**
 * Created by PhpStorm.
 * User: foxtech
 * Date: 17.06.18
 * Time: 23:57
 */

namespace App\Http\Controllers;


use App\Adapters\ItemAdapter;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * @param Request $request
     * @param ItemAdapter $adapter
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Request $request, ItemAdapter $adapter): RedirectResponse
    {
        $adapter->create(
            $request->id,
            $request->shipment,
            $request->name,
            $request->code
        );

        return back();
    }

    /**
     * @param Request $request
     * @param ItemAdapter $adapter
     * @return RedirectResponse
     */
    public function delete(Request $request, ItemAdapter $adapter): RedirectResponse
    {
        $adapter->delete($request->id);

        return back();
    }
}