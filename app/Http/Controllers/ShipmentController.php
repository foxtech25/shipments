<?php
/**
 * Created by PhpStorm.
 * User: foxtech
 * Date: 17.06.18
 * Time: 21:35
 */

namespace App\Http\Controllers;

use App\Adapters\ShipmentAdapter;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class ShipmentController
 * @package App\Http\Controllers
 */
class ShipmentController
{
    /**
     * @param ShipmentAdapter $adapter
     * @return View
     */
    public function show(ShipmentAdapter $adapter): View
    {
        $shipments = $adapter->show();

        return view('shipment.show', [
            'shipments' => $shipments['data']->shipments
        ]);
    }

    /**
     * @param Request $request
     * @param ShipmentAdapter $adapter
     * @return RedirectResponse
     */
    public function create(Request $request, ShipmentAdapter $adapter): RedirectResponse
    {
        $adapter->create($request->id, $request->name);

        return redirect('/shipment');
    }

    /**
     * @param Request $request
     * @param ShipmentAdapter $adapter
     * @return View
     */
    public function get(Request $request, ShipmentAdapter $adapter): View
    {
        $shipment = $adapter->get($request->id);

        return view('shipment.get', [
            'shipment' => $shipment['data']
        ]);
    }
}