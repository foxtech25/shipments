<?php
/**
 * Created by PhpStorm.
 * User: foxtech
 * Date: 17.06.18
 * Time: 16:06
 */

namespace App\Adapters;

use App\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

/**
 * Class UserAdapter
 * @package App\Adapters
 */
class ShipmentAdapter extends MainAdapter
{
    private const ROUTE = User::HOST . '/shipment';
    /**
     * @return array
     */
    public function show(): array
    {
        return $this->run('GET', self::ROUTE, $this->headers);
    }

    /**
     * @param int $id
     * @param string $name
     */
    public function create(int $id, string $name): void
    {
        $this->run('POST', self::ROUTE, array_merge(
            $this->headers,
            [
                'body' => \GuzzleHttp\json_encode([
                    'name' => $name,
                    'id'   => $id
                ])
            ]
        ));
    }

    /**
     * @param int $id
     * @return array
     */
    public function get(int $id): array
    {
        return $this->run('GET', self::ROUTE . '/' . $id, $this->headers);
    }
}