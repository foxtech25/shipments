<?php
/**
 * Created by PhpStorm.
 * User: foxtech
 * Date: 17.06.18
 * Time: 16:06
 */

namespace App\Adapters;

use App\User;
use Illuminate\Support\Facades\Redis;

/**
 * Class UserAdapter
 * @package App\Adapters
 */
class UserAdapter extends MainAdapter
{
    /**
     * @param string $email
     * @param string $password
     */
    public function login(string $email, string $password): void
    {
        $result = $this->run('POST', User::HOST . '/login', array_merge(
            $this->headers,
            [
                'body' => \GuzzleHttp\json_encode([
                    'email'    => $email,
                    'password' => $password
                ])
            ]
        ));

        Redis::set('token', array_shift($result['data'])->token, 'EX', 1800);
    }
}