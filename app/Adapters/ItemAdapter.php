<?php
/**
 * Created by PhpStorm.
 * User: foxtech
 * Date: 17.06.18
 * Time: 23:51
 */

namespace App\Adapters;

use App\User;

class ItemAdapter extends MainAdapter
{
    private const ROUTE = User::HOST . '/item';

    /**
     * @param int $id
     * @param int $shipmentId
     * @param string $name
     * @param string $code
     */
    public function create(int $id, int $shipmentId, string $name, string $code): void
    {
        $this->run('POST', self::ROUTE, array_merge(
            $this->headers,
            [
                'body' => \GuzzleHttp\json_encode([
                    'id'          => $id,
                    'shipment_id' => $shipmentId,
                    'name'        => $name,
                    'code'        => $code
                ])
            ]
        ));
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        $this->client->request('DELETE', self::ROUTE . '/' . $id, $this->headers);
    }
}