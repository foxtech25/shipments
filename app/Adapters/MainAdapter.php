<?php
/**
 * Created by PhpStorm.
 * User: foxtech
 * Date: 17.06.18
 * Time: 21:48
 */

namespace App\Adapters;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class MainAdapter
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * MainAdapter constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $option
     * @return array
     */
    protected function run(string $method, string $uri, array $option): array
    {
        return (array) \GuzzleHttp\json_decode(
            (string) $this->client
                ->request($method, $uri, $option)
                ->getBody()
        );
    }

    /**
     * @param $name
     * @return array|null
     */
    public function __get($name): ?array
    {
        if ($name == 'headers') {
            return [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer' . Redis::get('token')
                ]
            ];
        }

        return null;
    }
}