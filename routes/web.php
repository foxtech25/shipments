<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::post('/login', 'UserController@login');

Route::get('/shipment', 'ShipmentController@show')->middleware('api.token');
Route::post('/shipment', 'ShipmentController@create')->middleware('api.token')->name('shipment.create');
Route::get('/shipment/{id}', 'ShipmentController@get')->middleware('api.token')->name('shipment.get');

Route::post('/item', 'ItemController@create')->middleware('api.token')->name('item.create');
Route::get('/item/{id}', 'ItemController@delete')->middleware('api.token')->name('item.delete');


