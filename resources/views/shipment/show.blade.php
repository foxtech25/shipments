@extends('layouts.app')

@section('content')
    @foreach($shipments as $shipment)
        ID: <a href="{{ route('shipment.get', ['id' => $shipment->id]) }}">{{ $shipment->id }}</a>
        NAME: {{ $shipment->name }}
        <br>
    @endforeach

    <br>Create new:<br>
    <form method="POST" action="{{ route('shipment.create') }}">
        @csrf
        <input type="hidden" value="{{ array_pop($shipments)->id + 1 }}" name="id">
        Name: <input type="text" name="name">
        <button type="submit">Send</button>
    </form>
@endsection