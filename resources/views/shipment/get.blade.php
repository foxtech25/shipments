@extends('layouts.app')

@section('content')
    ID: {{ $shipment->id }}
    NAME: {{ $shipment->name }}
    <div>ITEMS:</div>
    @foreach($shipment->items as $item)
        id: {{ $item->id }}
        name: {{ $item->name }}
        <a href="{{ route('item.delete', ['id' => $item->id]) }}">Delete</a>
        <br>
    @endforeach

    <br>Create new item:<br>
    <form method="POST" action="{{ route('item.create') }}">
        @csrf
        Id: <input type="number" name="id">
        <input type="hidden" value="{{ $shipment->id }}" name="shipment">
        Name: <input type="text" name="name">
        Code: <input type="text" name="code">
        <button type="submit">Send</button>
    </form>
@endsection