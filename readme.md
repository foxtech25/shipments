How install:

git clone

cd laradock

cp env-example .env

docker-compose run -d nginx workspace redis

docker-compose exec workspace bash

composer install

cp .env.example .env